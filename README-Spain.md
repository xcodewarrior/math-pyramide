# Política de privacidad de Math Pyramide
## Última actualización 7/11/2021

Math Pyramide ("nosotros" o "nos" o "nuestro") respeta la privacidad de nuestros usuarios ("usuario" o "usted"). Esta Política de privacidad explica cómo recopilamos, usamos, divulgamos y protegemos su información cuando visita nuestra aplicación móvil (la "Aplicación"). Lea atentamente esta Política de privacidad. SI NO ESTÁ DE ACUERDO CON LOS TÉRMINOS DE ESTA POLÍTICA DE PRIVACIDAD, NO ACCEDA A LA APLICACIÓN.

Nos reservamos el derecho a realizar cambios en esta Política de privacidad en cualquier momento y por cualquier motivo. Le avisaremos sobre cualquier cambio actualizando la fecha de "Última actualización" de esta Política de privacidad. Le recomendamos que revise periódicamente esta Política de privacidad para mantenerse informado de las actualizaciones. Se considerará que usted ha sido informado, estará sujeto y se considerará que ha aceptado los cambios en cualquier Política de privacidad revisada por su uso continuo de la Aplicación después de la fecha en que se publique dicha Política de privacidad revisada.

### RECOPILACIÓN DE SU INFORMACIÓN

No tomamos ninguna información sobre el usuario.

Por ley o para proteger derechos

Si creemos que la divulgación de información sobre usted es necesaria para responder a un proceso legal, para investigar o remediar posibles violaciones de nuestras políticas, o para proteger los derechos, la propiedad y la seguridad de otros, podemos compartir su información según lo permita o requiera cualquier ley, norma o reglamento aplicable. Esto incluye el intercambio de información con otras entidades para la protección contra el fraude y la reducción del riesgo crediticio.

Proveedores de servicios de terceros

Podemos compartir su información con terceros que brindan servicios para nosotros o en nuestro nombre, incluido el procesamiento de pagos, el análisis de datos, la entrega de correo electrónico, los servicios de alojamiento, el servicio al cliente y la asistencia de marketing.

Comunicaciones de marketing

Con su consentimiento, o con la oportunidad de retirar su consentimiento, podemos compartir su información con terceros con fines de marketing, según lo permita la ley.

Interacciones con otros usuarios

Si interactúa con otros usuarios de la Aplicación, esos usuarios pueden ver su nombre, foto de perfil y descripciones de su actividad, incluido el envío de invitaciones a otros usuarios, chatear con otros usuarios, dar me gusta a las publicaciones, seguir blogs.

Publicaciones en línea

Cuando publica comentarios, contribuciones u otro contenido en las Aplicaciones, sus publicaciones pueden ser vistas por todos los usuarios y pueden distribuirse públicamente fuera de la Aplicación a perpetuidad.

Anunciantes de terceros

Podemos utilizar empresas de publicidad de terceros para publicar anuncios cuando visita la Aplicación. Estas empresas pueden utilizar información sobre sus visitas a la Aplicación y otros sitios web que se encuentran en las cookies web con el fin de proporcionar anuncios sobre bienes y servicios de su interés.

Afiliados

Podemos compartir su información con nuestros afiliados, en cuyo caso les exigiremos que cumplan con esta Política de privacidad. Los afiliados incluyen nuestra empresa matriz y cualquier subsidiaria, socios de empresas conjuntas u otras empresas que controlemos o que estén bajo control común con nosotros.

Compañeros de negocio

Podemos compartir su información con nuestros socios comerciales para ofrecerle ciertos productos, servicios o promociones.

Oferta de pared

La Aplicación puede mostrar un "muro de ofertas" alojado por terceros. Dicho muro de ofertas permite a los anunciantes externos ofrecer moneda virtual, obsequios u otros artículos a los usuarios a cambio de la aceptación y finalización de una oferta publicitaria. Dicho muro de ofertas puede aparecer en la Aplicación y mostrarse en función de ciertos datos, como su área geográfica o información demográfica. Cuando haga clic en un muro de ofertas, abandonará la Aplicación. Se compartirá un identificador único, como su identificación de usuario, con el proveedor del muro de ofertas para evitar el fraude y acreditar adecuadamente su cuenta.

Contactos de redes sociales

Si se conecta a la Aplicación a través de una red social, sus contactos en la red social verán su nombre, foto de perfil y descripciones de su actividad.

Otros Terceros

Podemos compartir su información con anunciantes e inversores con el fin de realizar análisis comerciales generales. También podemos compartir su información con dichos terceros con fines de marketing, según lo permita la ley.

Venta o quiebra

Si reorganizamos o vendemos todos o una parte de nuestros activos, nos sometemos a una fusión o somos adquiridos por otra entidad, podemos transferir su información a la entidad sucesora. Si cerramos o entramos en bancarrota, su información sería un activo transferido o adquirido por un tercero. Usted reconoce que tales transferencias pueden ocurrir y que el cesionario puede rechazar los compromisos de honor que asumimos en esta Política de privacidad.

No somos responsables de las acciones de terceros con los que comparte datos personales o confidenciales, y no tenemos autoridad para administrar o controlar las solicitudes de terceros. Si ya no desea recibir correspondencia, correos electrónicos u otras comunicaciones de terceros, es responsable de comunicarse con el tercero directamente.

### TECNOLOGÍAS DE SEGUIMIENTO

Cookies y balizas web

No utilizamos cookies ni balizas web.

Publicidad basada en Internet

No utilizamos ninguna herramienta publicitaria para la aplicación Apple Watch.

Análisis de sitios web

También podemos asociarnos con proveedores externos seleccionados, como Google Analytics, para permitir tecnologías de seguimiento y servicios de remarketing en la Aplicación mediante el uso de cookies propias y de terceros, para, entre otras cosas, analizar y rastrear el uso de los usuarios. de la Aplicación, determinar la popularidad de cierto contenido y comprender mejor la actividad en línea. Al acceder a la Aplicación, usted acepta la recopilación y el uso de su información por parte de estos proveedores externos. Le recomendamos que revise su política de privacidad y se comunique con ellos directamente para obtener respuestas a sus preguntas. No transferimos información personal a estos proveedores externos.

Debe tener en cuenta que adquirir una computadora nueva, instalar un navegador nuevo, actualizar un navegador existente o borrar o alterar los archivos de cookies de su navegador también puede borrar ciertas cookies, complementos o configuraciones de exclusión voluntaria.

### SITIOS WEB DE TERCEROS

No utilizamos sitios web de terceros.

### SEGURIDAD DE SU INFORMACIÓN

Usamos medidas de seguridad administrativas, técnicas y físicas para ayudar a proteger su información personal. Si bien hemos tomado medidas razonables para proteger la información personal que nos proporciona, tenga en cuenta que, a pesar de nuestros esfuerzos, ninguna medida de seguridad es perfecta o impenetrable, y ningún método de transmisión de datos puede garantizarse contra cualquier interceptación u otro tipo de uso indebido. Cualquier información divulgada en línea es vulnerable a la interceptación y el uso indebido por parte de terceros no autorizados. Por lo tanto, no podemos garantizar una seguridad completa si proporciona información personal.

### POLÍTICA PARA NIÑOS

No solicitamos a sabiendas información ni comercializamos a niños menores de 13 años. Si se entera de algún dato que hemos recopilado de niños menores de 13 años, comuníquese con nosotros utilizando la información de contacto que se proporciona a continuación.

### CONTROLES PARA CARACTERÍSTICAS DE NO SEGUIMIENTO

La mayoría de los navegadores web y algunos sistemas operativos móviles incluyen una función o configuración de No rastrear (“DNT”) que puede activar para indicar su preferencia de privacidad para que no se controlen y recopilen datos sobre sus actividades de navegación en línea. No se ha finalizado ningún estándar tecnológico uniforme para reconocer e implementar señales DNT. Como tal, actualmente no respondemos a las señales del navegador DNT ni a ningún otro mecanismo que comunique automáticamente su elección de no ser rastreado en línea. Si se adopta un estándar para el seguimiento en línea que debemos seguir en el futuro, le informaremos sobre esa práctica en una versión revisada de esta Política de privacidad.

### DERECHOS DE PRIVACIDAD DE CALIFORNIA

La Sección 1798.83 del Código Civil de California, también conocida como la ley "Shine The Light", permite a nuestros usuarios que son residentes de California solicitar y obtener de nosotros, una vez al año y de forma gratuita, información sobre las categorías de información personal (si corresponde) que nosotros divulgados a terceros con fines de marketing directo y los nombres y direcciones de todos los terceros con los que compartimos información personal en el año calendario inmediatamente anterior. Si usted es un residente de California y desea realizar una solicitud de este tipo, envíenos su solicitud por escrito utilizando la información de contacto que se proporciona a continuación.

Si es menor de 18 años, reside en California y tiene una cuenta registrada con la Aplicación, tiene derecho a solicitar la eliminación de los datos no deseados que publique públicamente en la Aplicación. Para solicitar la eliminación de dichos datos, comuníquese con nosotros utilizando la información de contacto que se proporciona a continuación e incluya la dirección de correo electrónico asociada con su cuenta y una declaración de que reside en California. Nos aseguraremos de que los datos no se muestren públicamente en la Aplicación, pero tenga en cuenta que es posible que los datos no se eliminen completa o completamente de nuestros sistemas.

### CONTÁCTENOS

Si tiene preguntas o comentarios sobre esta Política de privacidad, comuníquese con nosotros a:

emrahkrkmz1@gmail.com