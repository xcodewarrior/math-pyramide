# Politica sulla privacy della piramide matematica Pyramid
## Ultimo aggiornamento 07/11/2021

Math Pyramide ("noi" o "ci" o "nostro") rispetta la privacy dei nostri utenti ("utente" o "tu"). Questa Informativa sulla privacy spiega come raccogliamo, utilizziamo, divulghiamo e proteggiamo le tue informazioni quando visiti la nostra applicazione mobile (l'"Applicazione"). Si prega di leggere attentamente questa Informativa sulla privacy. SE NON SI ACCETTANO I TERMINI DELLA PRESENTE INFORMATIVA SULLA PRIVACY, SI PREGA DI NON ACCEDERE ALL'APPLICAZIONE.

Ci riserviamo il diritto di apportare modifiche alla presente Informativa sulla privacy in qualsiasi momento e per qualsiasi motivo. Ti avviseremo di eventuali modifiche aggiornando la data "Ultimo aggiornamento" della presente Informativa sulla privacy. Ti invitiamo a rivedere periodicamente questa Informativa sulla privacy per rimanere informato sugli aggiornamenti. Si riterrà che l'utente sia stato informato, sarà soggetto a e si riterrà che abbia accettato le modifiche a qualsiasi Informativa sulla privacy modificata dall'uso continuato dell'Applicazione dopo la data di pubblicazione di tale Informativa sulla privacy modificata.

### RACCOLTA DELLE TUE INFORMAZIONI

Non prendiamo alcuna informazione sull'utente.

Per legge o per proteggere i diritti

Se riteniamo che il rilascio di informazioni su di te sia necessario per rispondere a procedimenti legali, per indagare o porre rimedio a potenziali violazioni delle nostre politiche o per proteggere i diritti, la proprietà e la sicurezza di altri, potremmo condividere le tue informazioni come consentito o richiesto da qualsiasi legge, norma o regolamento applicabile. Ciò include lo scambio di informazioni con altre entità per la protezione dalle frodi e la riduzione del rischio di credito.

Fornitori di servizi di terze parti

Potremmo condividere le tue informazioni con terze parti che svolgono servizi per noi o per nostro conto, inclusi elaborazione dei pagamenti, analisi dei dati, consegna e-mail, servizi di hosting, servizio clienti e assistenza marketing.

Comunicazioni di marketing

Con il tuo consenso, o con l'opportunità di revocare il consenso, potremmo condividere le tue informazioni con terze parti per scopi di marketing, come consentito dalla legge.

Interazioni con altri utenti

Se interagisci con altri utenti dell'Applicazione, tali utenti possono vedere il tuo nome, la foto del profilo e le descrizioni della tua attività, incluso l'invio di inviti ad altri utenti, chattare con altri utenti, mettere mi piace ai post, seguire blog.

Inserzioni online

Quando pubblichi commenti, contributi o altri contenuti sulle Applicazioni, i tuoi post possono essere visualizzati da tutti gli utenti e possono essere distribuiti pubblicamente al di fuori dell'Applicazione in perpetuo

Inserzionisti di terze parti

Potremmo utilizzare società pubblicitarie di terze parti per pubblicare annunci quando visiti l'Applicazione. Queste società possono utilizzare le informazioni sulle tue visite all'Applicazione e ad altri siti Web contenute nei cookie Web al fine di fornire annunci pubblicitari su beni e servizi di tuo interesse.

Affiliati

Potremmo condividere le tue informazioni con i nostri affiliati, nel qual caso richiederemo a tali affiliati di onorare la presente Informativa sulla privacy. Le affiliate includono la nostra società madre e qualsiasi consociata, partner di joint venture o altre società che controlliamo o che sono sotto il nostro controllo comune.

Soci in affari

Potremmo condividere le tue informazioni con i nostri partner commerciali per offrirti determinati prodotti, servizi o promozioni.

Bacheca delle offerte

L'Applicazione può visualizzare un "muro di offerte" ospitato da terze parti. Tale bacheca di offerte consente agli inserzionisti di terze parti di offrire valuta virtuale, regali o altri articoli agli utenti in cambio dell'accettazione e del completamento di un'offerta pubblicitaria. Tale bacheca dell'offerta può apparire nell'Applicazione ed essere mostrata all'utente in base a determinati dati, come l'area geografica o le informazioni demografiche. Quando fai clic su una bacheca di offerte, uscirai dall'applicazione. Un identificatore univoco, come il tuo ID utente, verrà condiviso con il provider del muro di offerte al fine di prevenire le frodi e accreditare correttamente il tuo account.

Contatti sui social media

Se ti connetti all'Applicazione tramite un social network, i tuoi contatti sul social network vedranno il tuo nome, la foto del profilo e le descrizioni della tua attività.

Altre terze parti

Potremmo condividere le tue informazioni con inserzionisti e investitori allo scopo di condurre analisi commerciali generali. Potremmo anche condividere le tue informazioni con tali terze parti per scopi di marketing, come consentito dalla legge.

Vendita o fallimento

Se riorganizziamo o vendiamo tutto o parte dei nostri beni, subiamo una fusione o siamo acquisiti da un'altra entità, potremmo trasferire le tue informazioni all'entità successore. Se cessiamo l'attività o entriamo in bancarotta, le tue informazioni sarebbero un bene trasferito o acquisito da una terza parte. Riconosci che tali trasferimenti possono verificarsi e che il cessionario può rifiutare gli impegni di onorare che abbiamo assunto nella presente Informativa sulla privacy.

Non siamo responsabili delle azioni di terzi con cui condividi dati personali o sensibili e non abbiamo l'autorità per gestire o controllare le richieste di terzi. Se non desideri più ricevere corrispondenza, e-mail o altre comunicazioni da terze parti, sei responsabile di contattare direttamente la terza parte.

### TECNOLOGIE DI TRACCIAMENTO

Cookie e Web Beacon

Non utilizziamo cookie e web beacon

Pubblicità basata su Internet

Non utilizziamo alcuno strumento pubblicitario per l'applicazione Apple Watch.

Analisi del sito web

Potremmo anche collaborare con fornitori di terze parti selezionati, come Google Analytics per consentire tecnologie di tracciamento e servizi di remarketing sull'Applicazione attraverso l'uso di cookie proprietari e cookie di terze parti, per, tra le altre cose, analizzare e tenere traccia dell'utilizzo degli utenti dell'Applicazione, determinare la popolarità di determinati contenuti e comprendere meglio l'attività online. Accedendo all'Applicazione, acconsenti alla raccolta e all'utilizzo delle tue informazioni da parte di questi fornitori di terze parti. Siete incoraggiati a rivedere la loro politica sulla privacy e contattarli direttamente per le risposte alle vostre domande. Non trasferiamo informazioni personali a questi fornitori di terze parti.

È necessario essere consapevoli del fatto che l'acquisto di un nuovo computer, l'installazione di un nuovo browser, l'aggiornamento di un browser esistente o la cancellazione o la modifica in altro modo dei file dei cookie del browser possono anche eliminare alcuni cookie di disattivazione, plug-in o impostazioni.

### SITI WEB DI TERZE PARTI

Non utilizziamo siti Web di terze parti.

### SICUREZZA DELLE TUE INFORMAZIONI

Utilizziamo misure di sicurezza amministrative, tecniche e fisiche per proteggere le tue informazioni personali. Sebbene abbiamo adottato misure ragionevoli per proteggere le informazioni personali che ci fornisci, tieni presente che, nonostante i nostri sforzi, nessuna misura di sicurezza è perfetta o impenetrabile e nessun metodo di trasmissione dei dati può essere garantito contro qualsiasi intercettazione o altro tipo di uso improprio. Qualsiasi informazione divulgata online è vulnerabile all'intercettazione e all'uso improprio da parte di soggetti non autorizzati. Pertanto, non possiamo garantire la completa sicurezza se fornisci informazioni personali.

### POLITICA PER I BAMBINI

Non richiediamo consapevolmente informazioni o commercializziamo bambini di età inferiore ai 13 anni. Se vieni a conoscenza di dati che abbiamo raccolto da bambini di età inferiore a 13 anni, ti preghiamo di contattarci utilizzando le informazioni di contatto fornite di seguito.

### CONTROLLI PER LE FUNZIONI NON TRACCIARE

La maggior parte dei browser Web e alcuni sistemi operativi mobili includono una funzione o un'impostazione Do-Not-Track ("DNT") che puoi attivare per segnalare la tua preferenza sulla privacy per non avere dati sulle tue attività di navigazione online monitorati e raccolti. Non è stato finalizzato alcuno standard tecnologico uniforme per il riconoscimento e l'implementazione dei segnali DNT. Pertanto, al momento non rispondiamo ai segnali del browser DNT o a qualsiasi altro meccanismo che comunichi automaticamente la tua scelta di non essere tracciato online. Se viene adottato uno standard per il monitoraggio online che dobbiamo seguire in futuro, ti informeremo su tale pratica in una versione rivista della presente Informativa sulla privacy.

### DIRITTI SULLA PRIVACY DELLA CALIFORNIA

La sezione 1798.83 del codice civile della California, nota anche come legge "Shine The Light", consente ai nostri utenti residenti in California di richiedere e ottenere da noi, una volta all'anno e gratuitamente, informazioni su categorie di dati personali (se presenti) che noi divulgati a terzi per scopi di marketing diretto e i nomi e gli indirizzi di tutti i terzi con cui abbiamo condiviso le informazioni personali nell'anno solare immediatamente precedente. Se sei residente in California e desideri effettuare tale richiesta, inviaci la tua richiesta per iscritto utilizzando le informazioni di contatto fornite di seguito.

Se hai meno di 18 anni, risiedi in California e hai un account registrato con l'Applicazione, hai il diritto di richiedere la rimozione dei dati indesiderati che pubblichi pubblicamente sull'Applicazione. Per richiedere la rimozione di tali dati, contattaci utilizzando le informazioni di contatto fornite di seguito e includi l'indirizzo email associato al tuo account e una dichiarazione che risiedi in California. Faremo in modo che i dati non vengano visualizzati pubblicamente sull'Applicazione, ma tieni presente che i dati potrebbero non essere completamente o completamente rimossi dai nostri sistemi.

### CONTATTACI

In caso di domande o commenti sulla presente Informativa sulla privacy, contattaci all'indirizzo:

emrahkrkmz1@gmail.com