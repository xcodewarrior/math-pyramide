# 数学金字塔隐私政策
## 上次更新时间：2021 年 11 月 7 日

Math Pyramide（“我们”或“我们”或“我们的”）尊重我们用户（“用户”或“您”）的隐私。本隐私政策解释了当您访问我们的移动应用程序（“应用程序”）时，我们如何收集、使用、披露和保护您的信息。请仔细阅读本隐私政策。如果您不同意本隐私政策的条款，请不要访问该应用程序。

我们保留随时以任何理由更改本隐私政策的权利。我们将通过更新本隐私政策的“上次更新”日期来提醒您任何更改。我们鼓励您定期查看本隐私政策以随时了解更新。在修订的隐私政策发布之日后继续使用本应用程序，您将被视为已获悉、将受制于并被视为已接受任何修订的隐私政策中的更改。

### 收集您的信息

我们不会获取有关用户的任何信息。

根据法律或保护权利

如果我们认为为了响应法律程序、调查或补救可能违反我们政策的行为，或保护他人的权利、财产和安全，有必要发布有关您的信息，我们可能会在允许或要求任何适用的法律、规则或法规。这包括与其他实体交换信息以防止欺诈和降低信用风险。

第三方服务提供商

我们可能会与为我们或代表我们提供服务的第三方共享您的信息，包括支付处理、数据分析、电子邮件发送、托管服务、客户服务和营销协助。

营销传播

经您同意或有机会让您撤回同意，我们可能会在法律允许的范围内与第三方共享您的信息以用于营销目的。

与其他用户的互动

如果您与应用程序的其他用户互动，这些用户可能会看到您的姓名、个人资料照片和您的活动描述，包括向其他用户发送邀请、与其他用户聊天、喜欢帖子、关注博客。

在线帖子

当您向应用程序发表评论、贡献或其他内容时，您的帖子可能会被所有用户查看，并且可能会永久在应用程序之外公开分发

第三方广告商

当您访问应用程序时，我们可能会使用第三方广告公司来投放广告。这些公司可能会使用有关您访问应用程序和包含在网络 cookie 中的其他网站的信息来提供有关您感兴趣的商品和服务的广告。

关联公司

我们可能会与我们的附属公司共享您的信息，在这种情况下，我们将要求这些附属公司遵守本隐私政策。关联公司包括我们的母公司以及我们控制或与我们共同控制的任何子公司、合资伙伴或其他公司。

商业合作伙伴

我们可能会与我们的业务合作伙伴共享您的信息，以便为您提供某些产品、服务或促销活动。

优惠墙

应用程序可能会显示第三方托管的“优惠墙”。这种优惠墙允许第三方广告商向用户提供虚拟货币、礼物或其他物品，以换取接受和完成广告优惠。此类优惠墙可能会出现在应用程序中，并根据某些数据（例如您的地理区域或人口统计信息）向您显示。当您单击报价墙时，您将离开应用程序。唯一标识符，例如您的用户 ID，将与优惠墙提供商共享，以防止欺诈并正确记入您的帐户。

社交媒体联系人

如果您通过社交网络连接到应用程序，您在社交网络上的联系人将看到您的姓名、个人资料照片和您的活动描述。

其他第三方

我们可能会与广告商和投资者共享您的信息，以进行一般业务分析。在法律允许的情况下，我们也可能出于营销目的与此类第三方共享您的信息。

出售或破产

如果我们重组或出售我们的全部或部分资产、进行合并或被另一实体收购，我们可能会将您的信息转移到继任实体。如果我们停业或破产，您的信息将成为第三方转让或获得的资产。您承认此类转让可能会发生，并且受让人可能会拒绝我们在本隐私政策中做出的兑现承诺。

我们不对您与之共享个人或敏感数据的第三方的行为负责，并且我们无权管理或控制第三方的请求。如果您不再希望收到来自第三方的信函、电子邮件或其他通信，您有责任直接与第三方联系。

### 追踪技术

Cookie 和网络信标

我们不使用任何 cookie 和网络信标

基于互联网的广告

我们不为 Apple Watch 应用程序使用任何广告工具。

网站分析

我们还可能与选定的第三方供应商合作，例如 Google Analytics，以允许通过使用第一方 cookie 和第三方 cookie 在应用程序上跟踪技术和再营销服务，以分析和跟踪用户的使用等应用程序，确定某些内容的受欢迎程度，并更好地了解在线活动。通过访问应用程序，您同意这些第三方供应商收集和使用您的信息。我们鼓励您查看他们的隐私政策并直接与他们联系以回答您的问题。我们不会将个人信息传输给这些第三方供应商。

您应该知道，购买新计算机、安装新浏览器、升级现有浏览器或删除或以其他方式更改浏览器的 cookie 文件也可能会清除某些选择退出的 cookie、插件或设置。

### 第三方网站

我们不使用任何第三方网站。

###您信息的安全性

我们使用管理、技术和物理安全措施来帮助保护您的个人信息。虽然我们已采取合理措施来保护您提供给我们的个人信息，但请注意，尽管我们做出了努力，但没有任何安全措施是完美或不可渗透的，并且无法保证任何数据传输方法都不会被拦截或其他类型的滥用。任何在线披露的信息都容易被未授权方拦截和滥用。因此，如果您提供个人信息，我们无法保证完全安全。

### 儿童政策

我们不会故意向 13 岁以下儿童索取信息或向其推销。如果您知道我们从 13 岁以下儿童那里收集的任何数据，请使用下面提供的联系信息与我们联系。

### 禁止追踪功能的控制

大多数网络浏览器和某些移动操作系统都包含 Do-Not-Track (“DNT”) 功能或设置，您可以激活以表明您的隐私偏好，不让有关您的在线浏览活动的数据受到监控和收集。尚未最终确定识别和实施 DNT 信号的统一技术标准。因此，我们目前不响应 DNT 浏览器信号或任何其他自动传达您选择不在线跟踪的机制。如果采用了我们将来必须遵循的在线跟踪标准，我们将在本隐私政策的修订版中告知您该做法。

### 加州隐私权

加利福尼亚州民法典第 1798.83 节，也称为“闪耀之光”法，允许我们的加利福尼亚居民用户每年一次免费向我们请求和获取有关我们的个人信息类别（如果有）的信息出于直接营销目的而向第三方披露，以及我们在前一个日历年与其共享个人信息的所有第三方的名称和地址。如果您是加利福尼亚州居民并希望提出此类请求，请使用下面提供的联系信息以书面形式向我们提交您的请求。

如果您未满 18 岁，居住在加利福尼亚州，并且拥有应用程序的注册帐户，则您有权要求删除您在应用程序上公开发布的不需要的数据。要请求删除此类数据，请使用下面提供的联系信息与我们联系，并附上与您的帐户相关联的电子邮件地址以及您居住在加利福尼亚州的声明。我们将确保数据不会公开显示在应用程序上，但请注意，数据可能不会完全或全面地从我们的系统中删除。

＃＃＃ 联系我们

如果您对本隐私政策有任何疑问或意见，请通过以下方式与我们联系：

emrahkrkmz1@gmail.com